import scala.concurrent.Future
import scala.util.{Success, Try, Failure}

final case class UserPayload(id: Option[String], firstname: Option[String], surname: Option[String], age: Option[Int])

final case class User(id: String, firstname: String, surname: String, age: Int)

sealed trait Error
case object UserNotFound extends Error

class SomeAnnoyingJavaException(msg: String) extends Exception(msg)

def validateFirstname(firstname: String): Try[String] = if(firstname.nonEmpty) Success(firstname) else Failure(new SomeAnnoyingJavaException("First name is empty"))
def validateSurname(surname: String): Try[String] = if(surname.nonEmpty) Success(surname) else Failure(new SomeAnnoyingJavaException("Surname is empty"))
def validateAge(age: Int): Try[Int] = if(age > 0) Success(age) else Failure(new SomeAnnoyingJavaException("Age must be positive"))

def findUser(id: String): Future[Either[Error, User]] =
  if(id == "johndoe") {
    Future.successful(Right(User("johndoe", "John", "Doe", 23)))
  } else {
    Future.successful(Left(UserNotFound))
  }
def saveUser(user: User): Future[User] = Future.successful(user)
def sendBroadcast(user: User): Future[User] = Future.successful(user)

def serialize(user: User): String = s"""{"id":"${user.id}","firstname":"${user.firstname}","surname":"${user.surname}","age":"${user.age}"}"""


// TODO
// this should validate first name, surname and age from the payload
// then try to find user by id if provided
// - if user is found, then it should be updated, saved, broadcasted and serialized
//   -- for updates, if the value is provided in the payload then it should overwrite existing, if it's not provided then it shouldn't overwrite the existing value
// - if user is not found, then new user should be created, saved, broadcasted and serialized
//   -- for creates, if not all fields are available in the payload which are required for user creation, then we should fail the request
//
// Unit tests are welcome
//
// You are free to use any libraries you want but shouldn't change any definitions above the TODO block (even though they are not ideal)
def processUserUpsertRequest(payload: UserPayload) = ???
